<?php

namespace App\Http\Controllers;

use App\Models\User as ModelsUser;
use Illuminate\Http\Request;

class User extends Controller
{
    //
    public function login(Request $request) {
        $user = [
            'email' => $request->email, 
            'password' => $request->password, 
        ];

        $users = ModelsUser::where('email',$user["email"])->first(); 
        if($users) {
            $users->where('password', 'password')->first();
            $token = $users->createToken('loginUser');
        }

        return response()->json([
            'token' => $token
        ]);
    }
}
